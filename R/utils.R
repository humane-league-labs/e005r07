#' pipe operator
#'
#' See \code{magrittr::\link[magrittr]{\%>\%}} for details.
#'
#' @name %>%
#' @rdname operator_pipe
#' @keywords internal
#' @export
#' @importFrom magrittr %>%
#' @usage lhs \%>\% rhs
NULL

#' exposition pipe operator
#'
#' See \code{magrittr::\link[magrittr]{\%$\%}} for details.
#'
#' @name %$%
#' @rdname operator_exposition
#' @keywords internal
#' @export
#' @importFrom magrittr %$%
#' @usage lhs \%$\% rhs
NULL

#' compound assignment pipe operator
#'
#' See \code{magrittr::\link[magrittr]{\%<>\%}} for details.
#'
#' @name %<>%
#' @rdname operator_compound
#' @keywords internal
#' @export
#' @importFrom magrittr %<>%
#' @usage lhs \%<>\% rhs
NULL

#' Print authors in APA style
#'
#' Helper function that takes as argument a person object as used in
#'   `DESCRIPTION` files as argument and returns the persons formatted as
#'   authors for a citation in APA style.
#'
#' @param persons object of class "person"
#'
#' @return character. The authors, formatted for a citation in APA style.
print_authors <- function(persons) {
  authors <- purrr::map_chr(
    persons,
    ~ paste(.x$family, .x$given, sep = ", ")
  )

  if (length(authors) > 2){
    glue::glue(
      paste0(authors[-length(authors)], collapse = ", "),
      ", & {authors[length(authors)]}"
    )
  } else if (length(authors) == 2){
    glue::glue(
      "{authors[1]} & {authors[2]}"
    )
  } else {
    glue::glue(
      "{authors}"
    )
  }
}

#' Print citation in APA style
#'
#' Helper function that generates a citation in APA style from the given inputs.
#'   To be used internally to generate citations for the R package and the
#'   associated publication
#'
#' @param authors object of class person. The authors.
#' @param date character. The publication date.
#' @param title character. The title.
#' @param pre character. Optional string to add before title.
#' @param post character. Optional string to add after title.
#' @param doi character. The publication's DOI.
#' @param markdown logical. Whether to format the returned string using
#'   markdown.
#'
#' @return character. The citation in APA style.
print_citation <- function(authors,
                           date,
                           title,
                           pre = "",
                           post = ".",
                           doi = "",
                           markdown = FALSE) {
  glue::glue(
    '
    {print_authors(authors)} \\
    ({date}). \\
    {ifelse(
      markdown,
      glue::glue("*{pre}{title}*{post}"),
      glue::glue("{pre}{title}{post}")
    )}\\
    {ifelse(doi == "", "", glue::glue(" {doi}"))}
    '
  )
}

#' Export package environment
#'
#' Exports the package environment, which holds several variables like the
#'   name of the package, authors, title, etc. for use in other functions.
#'
#' @return The package environment
#' @export
export_pkgenv <- function() {
  pkgenv
}
