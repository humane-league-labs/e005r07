% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/reproduce.R
\name{compute_hashes}
\alias{compute_hashes}
\title{Compute hashes}
\usage{
compute_hashes(dir)
}
\arguments{
\item{dir}{character. File path.}
}
\description{
Helper function that computes md5 hashes for all files in specified
directory.
}
