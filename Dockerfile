FROM rocker/tidyverse:3.6.0

# required
MAINTAINER Janosch Linkersdörfer <linkersdoerfer@mailbox.org>

## LaTeX configuration
## from https://github.com/rocker-org/rocker-versioned/blob/master/verse/3.6.0.Dockerfile, but newer CTAN repo

# Version-stable CTAN repo from the tlnet archive
ARG CTAN_REPO=${CTAN_REPO:-http://www.texlive.info/tlnet-archive/2020/06/01/tlnet}
ENV CTAN_REPO=${CTAN_REPO}

ENV PATH=$PATH:/opt/TinyTeX/bin/x86_64-linux/

## Add LaTeX, rticles and bookdown support
RUN wget "https://travis-bin.yihui.name/texlive-local.deb" \
  && dpkg -i texlive-local.deb \
  && rm texlive-local.deb \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
    ## for rJava
    default-jdk \
    ## Nice Google fonts
    fonts-roboto \
    ## used by some base R plots
    ghostscript \
    ## used to build rJava and other packages
    libbz2-dev \
    libicu-dev \
    liblzma-dev \
    ## system dependency of hunspell (devtools)
    libhunspell-dev \
    ## system dependency of hadley/pkgdown
    libmagick++-dev \
    ## rdf, for redland / linked data
    librdf0-dev \
    ## for V8-based javascript wrappers
    libv8-dev \
    ## R CMD Check wants qpdf to check pdf sizes, or throws a Warning
    qpdf \
    ## For building PDF manuals
    texinfo \
    ## for git via ssh key
    ssh \
 ## just because
    less \
    vim \
 ## parallelization
    libzmq3-dev \
    libopenmpi-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/ \
  ## Use tinytex for LaTeX installation
  && install2.r --error tinytex \
  ## Admin-based install of TinyTeX:
  && wget -qO- \
    "https://github.com/yihui/tinytex/raw/master/tools/install-unx.sh" | \
    sh -s - --admin --no-path \
  && mv ~/.TinyTeX /opt/TinyTeX \
  && if /opt/TinyTeX/bin/*/tex -v | grep -q 'TeX Live 2016'; then \
      ## Patch error handling of tlmgr path (https://tex.stackexchange.com/a/314079)
      ## in the frozen TeX Live 2016 snapshot by back-porting the corresponding fix:
      ## https://git.texlive.info/texlive/commit/Master/tlpkg/TeXLive/TLUtils.pm?id=69cee5e1ce4b20f6ebb6af77e19d49706a842a3e
      apt-get update && apt-get install -y --no-install-recommends patch \
      && wget -qO- \
         "https://git.texlive.info/texlive/patch/Master/tlpkg/TeXLive/TLUtils.pm?id=69cee5e1ce4b20f6ebb6af77e19d49706a842a3e" | \
         patch -i - /opt/TinyTeX/tlpkg/TeXLive/TLUtils.pm \
      && apt-get remove --purge --autoremove -y patch \
      && apt-get clean && rm -rf /var/lib/apt/lists/; \
    fi \
  && /opt/TinyTeX/bin/*/tlmgr path add \
  && tlmgr install metafont mfware inconsolata tex ae parskip listings \
  ## LaTeX packages for THLL template
  && tlmgr install extsizes babel-english fp ms pgf changepage hyphenat tcolorbox environ trimspaces wrapfig setspace enumitem psnfss symbol textcase libertinus-otf libertinus-fonts fpl mathpazo palatino sourcecodepro fancyhdr lastpage footmisc titlesec colortbl textpos caption newfloat biblatex logreq biblatex-apa preprint xifthen ifmtarg datetime fmtcount footnotebackref microtype csquotes ragged2e biber.x86_64-linux biber \
  && tlmgr path add \
  && Rscript -e "tinytex::r_texmf()" \
  && chown -R root:staff /opt/TinyTeX \
  && chown -R root:staff /usr/local/lib/R/site-library \
  && chmod -R g+w /opt/TinyTeX \
  && chmod -R g+wx /opt/TinyTeX/bin \
  && echo "PATH=${PATH}" >> /usr/local/lib/R/etc/Renviron \
  && install2.r --error PKI \
  ## And some nice R packages for publishing-related stuff
  && install2.r --error --deps TRUE \
    bookdown rticles rmdshower rJava


## Install newer pandoc version (> 2.8)
RUN rm /usr/local/bin/pandoc \
  && rm /usr/local/bin/pandoc-citeproc \
  && wget "https://github.com/jgm/pandoc/releases/download/2.10.1/pandoc-2.10.1-linux-amd64.tar.gz" \
  && tar xvzf ./pandoc-2.10.1-linux-amd64.tar.gz --strip-components 2 -C /usr/local/bin/


## binder configuration
## from: https://github.com/rocker-org/binder/blob/master/3.5.0/Dockerfile

ENV NB_USER rstudio
ENV NB_UID 1000
ENV VENV_DIR /srv/venv

# Set ENV for all programs...
ENV PATH ${VENV_DIR}/bin:$PATH
# And set ENV for R! It doesn't read from the environment...
RUN echo "PATH=${PATH}" >> /usr/local/lib/R/etc/Renviron

# The `rsession` binary that is called by nbrsessionproxy to start R doesn't
# seem to start without this being explicitly set
ENV LD_LIBRARY_PATH /usr/local/lib/R/lib

ENV HOME /home/${NB_USER}
WORKDIR ${HOME}

RUN apt-get update && \
    apt-get -y install python3-venv python3-dev && \
    apt-get purge && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Create a venv dir owned by unprivileged user & set up notebook in it
# This allows non-root to install python libraries if required
RUN mkdir -p ${VENV_DIR} && chown -R ${NB_USER} ${VENV_DIR}

USER ${NB_USER}
RUN python3 -m venv ${VENV_DIR} && \
    # Explicitly install a new enough version of pip
    pip3 install pip==9.0.1 && \
    pip3 install --no-cache-dir \
         nbrsessionproxy==0.6.1 && \
    jupyter serverextension enable --sys-prefix --py nbrsessionproxy && \
    jupyter nbextension install    --sys-prefix --py nbrsessionproxy && \
    jupyter nbextension enable     --sys-prefix --py nbrsessionproxy

RUN R --quiet -e "devtools::install_github('IRkernel/IRkernel')" && \
    R --quiet -e "IRkernel::installspec(prefix='${VENV_DIR}')"

CMD jupyter notebook --ip 0.0.0.0


## compendium-specific configuration

# install dependencies using `renv`
ENV RENV_VERSION 0.10.0
# RUN R -e "install.packages('remotes', repos = c(CRAN = 'https://cloud.r-project.org'))"
RUN R -e "devtools::install_github('rstudio/renv@${RENV_VERSION}')"
WORKDIR /project
COPY renv.lock renv.lock
RUN R -e 'renv::restore()'

# install research compendium
COPY . /e005r07

RUN . /etc/environment \
    && R -e "devtools::install('/e005r07', dep=FALSE)"

