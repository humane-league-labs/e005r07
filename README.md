
<!-- README.md is generated from README.Rmd. Please edit that file! -->

E005R07 <img src="man/figures/thll-logo.png" align="right" alt="THLL logo" width="200" style = "border: none; float: right;">
=============================================================================================================================

This repository contains the research compendium for The Humane League
Labs (THLL) report E005R07:

> Linkersdörfer, Janosch & Peacock, Jacob (2020). *Is Animal Cruelty or
> Abolitionist Messaging More Effective: A Reanalysis*.
> <a href="https://doi.org/10.31219/osf.io/x65h2" class="uri">https://doi.org/10.31219/osf.io/x65h2</a>

Contents
--------

The compendium contains all data, code, and text associated with the The
Humane League Labs (THLL) report E005R07. It is organized as follows:

-   [:file\_folder: R/](/R/) contains all the R code.
-   [:file\_folder:
    inst/analysis-dir/.drake/](/inst/analysis_dir/.drake/) contains the
    computational results cached with the R pipeline toolkit
    [`drake`](https://docs.ropensci.org/drake/).
-   [:file\_folder:
    inst/analysis-dir/analysis/](/inst/analysis_dir/analysis/) contains
    all files associated with the analysis.
    -   [:file\_folder: data/](/inst/analysis-dir/analysis/data): Raw
        data used in the analysis.
    -   [:file\_folder: report/](/inst/analysis-dir/analysis/report)
        -   [:file\_folder:
            report/](/inst/analysis-dir/analysis/report/report): R
            Markdown source document and support files to render the
            report as well as the rendered PDF document.
        -   [:file\_folder:
            suppl-mat/](/inst/analysis-dir//analysis/report/suppl-mat):
            R Markdown source document and support files to render the
            supplementary materials as well as the rendered HTML
            document.

How to install the compendium
-----------------------------

There are several ways to evaluate the research compendium’s contents
and reproduce the analysis.

#### 1. Reproduce the analysis locally

*Install the compendium and all its dependencies and reproduce the
analysis on your own computer.*

This approach assumes the following software dependencies:

-   R (version 3.6.0)
-   Pandoc (version ≥ 2.8)

The research compendium’s R package dependencies can be installed using
the R package manager
[`renv`](https://rstudio.github.io/renv/articles/renv.html) by executing
the following commands:

    # install the `devtools` package if is not already present
    if (!"devtools" %in% installed.packages()) install.packages("devtools")

    # download `renv` configuration file
    download.file("https://gitlab.com/humane-league-labs/e005r07/-/raw/master/renv.lock", "renv.lock")

    # (install and) initialize `renv`
    devtools::source_url("https://gitlab.com/humane-league-labs/e005r07/-/raw/master/renv/activate.R")

    # install R package dependencies
    renv::restore(prompt = FALSE)

The LaTeX dependencies to render the report to PDF can then be installed
as by executing

    thll::install_dependencies()

Finally, the research compendium can be installed as an R package from
GitLab by executing

    devtools::install_gitlab("humane-league-labs/e005r07", dependencies = FALSE)

#### 2. Reproduce the analysis locally inside a Docker container

*Replicate the exact computational environment used by the authors and
reproduce the analysis on your own computer.*

The compendium includes a Dockerfile that specifies how to build a
[Docker](https://www.docker.com/) image that contains the operating
system, libraries, software and R package dependencies, as well as the
research compendium, ready to use. After [installing
Docker](https://docs.docker.com/install/#supported-platforms) (needs
administrator privileges) and starting the installed application, one
can simply download the image and run the container using:

    docker run -p 8888:8888 registry.gitlab.com/humane-league-labs/e005r07

-   This command will print several URLs to the terminal.
-   Copy the URL starting with `https://127.0.0.1` and paste it into a
    web browser.
-   This will open a Jupyter Notebook.
-   Start an interactive RStudio session in the browser by selecting:

<img src="man/figures/rstudio-session.jpg" width="30%" style="display: block; margin: auto;" />

#### 3. Reproduce the analysis in the cloud

*Without having to install any software, reproduce the analysis in the
exact computational environment used by the authors.*

The same Docker container as above can be run using BinderHub on
[mybinder.org](https://mybinder.org/): Click
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/humane-league-labs%2Fe005r07/master?urlpath=rstudio)
to launch an interactive RStudio session in your web browser.

How to use the compendium
-------------------------

The R package provides functionality to

-   check the analysis for consistency
-   inspect the analysis pipeline and intermediate objects/results
-   reproduce the analysis results on the user’s computer

To get started, execute

    library(E005R07)
    reproduce_analysis()

Consult the
[documentation](https://humane-league-labs.gitlab.io/e005r07/articles/E005R07.html)
for details on how to use the package and its functions.

### Licenses

**Text and figures :**
[CC-BY-4.0](http://creativecommons.org/licenses/by/4.0/)

**Code :** [MIT](https://opensource.org/licenses/mit-license.php)

**Data :** [CC-0](http://creativecommons.org/publicdomain/zero/1.0/)
attribution requested in reuse
